# Instruções
- Clone do diretorio do projeto ou faço o download do arquivo.
- Rode o servidor no diretorio /public.
- Na primeira vez que vc acessar o projeto você será direcionado a pagina de configuração, onde voce pode instalar um projeto de exemplo, ou importar uma lista csv.
- O sistema utiliza SQLITE sendo assim, não será necessário configurar servidor de banco de dados.

# Disclaimer
- Esse projeto foi feito utilizando somente php (coisa  que não fazia a muito tempo) sem a ajuda de outra biblioteca ou algo do tipo.

Qualquer dúvida sobre o projeto, fique a vontade para entrar em contato.
