<?php require __DIR__.'/../components/header.php'; ?>
<div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="/add-product" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
        
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach ($allProd as $p) { ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $p['nome'];?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $p['sku'];?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $p['preco'];?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $p['quantidade'];?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $p['categoria'];?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
              <div class="action edit"><a href="/edit-product?id=<?= $p['sku'] ;?>"><span>Edit</span></a></div>
              <div class="action delete"><a href="/del-product?id=<?= $p['sku'] ;?>"><span>Delete</span></a></div>
          </div>
        </td>
      </tr>
      <?php } ?>
      
    </table>

<?php require __DIR__.'/../components/footer.php'; ?>