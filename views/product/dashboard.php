<?php require __DIR__.'/../components/header.php'; ?>
<div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
        You have  <?= count($allProd); ?>  products added on this store: <a href="add-product" class="btn-action">Add new Product</a>
    </div>
    <ul class="product-list">
        <?php foreach ($allProd as $p) { ?>
      <li>
        <div class="product-image">
          <img src="assets/images/product/<?= !isset($p['image']) ? 'tenis-2d-shoes.png' : $p['image'] ;?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?= $p['nome'] ;?></span></div>
          <div class="product-price"><span class="special-price"><?= $p['quantidade'] ;?> available</span> <span>R$<?=  $p['preco'] ?></span></div>
        </div>
      </li>
      <?php } ;?>
    </ul>
<?php require __DIR__.'/../components/footer.php'; ?>