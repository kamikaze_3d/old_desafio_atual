<?php require __DIR__.'/../components/header.php'; ?>
<h1 class="title new-item">New Product</h1>
<form method="POST" action="<?= isset($query) ? '/up-product' :'/save-product' ?>" enctype="multipart/form-data" >
      <div class="input-field">
        <label for="img" class="label">Product Image</label>
        <input type="file" id="img" name="image" value="<?= isset($image) ? '$image' : 'tenis-basket-light.png' ;?>" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="nome" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="preco" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantidade" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="categoria" class="input-text">
            <option value="category 1">Category 1</option>
          <option value="category 2">Category 2</option>
          <option value="category 3">Category 3</option>
          <option value="category 4">Category 4</option>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="descricao" class="input-text"></textarea>
      </div>
      <div class="actions-form">
        <a href="/products" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
<?php require __DIR__.'/../components/footer.php'; ?>