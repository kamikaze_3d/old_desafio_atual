<?php require __DIR__ . '/../components/header.php'; ?>
<h1 class="title new-item">New Category</h1>

<form method="POST" action="<?= isset($query) ? '/update-category' : '/save-category' ;?>" >
    <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" value="<?= isset($query) ? $query['nome'] : '' ?>" id="category-name" name="nome" class="input-text" />

    </div>
        <input type="hidden" id="category-code" value="<?= isset($query) ? $query['id'] : '' ?>" name="id" class="input-text" />
    <div class="actions-form">
        <a href="/categories" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
    </div>
</form>
<?php require __DIR__ . '/../components/footer.php'; ?>