<?php require __DIR__.'/../components/header.php'; ?>
<div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="new-category" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php
      foreach ($allCat as $cat) {
      ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $cat['nome'];?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $cat['id'];?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
              <div class="action edit"><span><a href="/edit-category?id=<?= $cat['id'];?>" >Edit</a></span></div>
              <div class="action delete"><span><a href="/del-category?id=<?= $cat['id'];?>" >Delete</a></span></div>
          </div>
        </td>
      </tr>
      <?php } ?>
    </table>
  
<?php require __DIR__.'/../components/footer.php'; ?>