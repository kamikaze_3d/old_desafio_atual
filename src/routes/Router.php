<?php

require __DIR__ . '/../autoloader.php';

require __DIR__ . '/../controller/CategoryController.php';
require __DIR__ . '/../controller/ProductController.php';

$db = __DIR__ . '/../../config/database/gojump.sqlite';

$urlPath = $_SERVER['REQUEST_URI'];

$product = new ProductController();
$category = new CategoryController();

if (is_file($db)) {
    switch ($urlPath) {

        case "/":
            $product->home();
            break;
        case "/dashboard":
            $product->home();
            break;
        case "/products":
            $product->load();
            break;
        case "/add-product":
            $product->new();
            break;
        case "/save-product":
            $diretorio = "assets/images/product/";
            chmod($diretorio, 0755);
            move_uploaded_file($_FILES['image']["tmp_name"], $diretorio . $_FILES['image']["name"]);    
            $_POST['image'] = $_FILES['image']["name"];
            
            $nome = $_POST['nome'];
            $descricao = $_POST['descricao'];
            $quantidade = $_POST['quantidade'];
            $preco = $_POST['preco'];
            $categoria = $_POST['categoria'];
            $image = $_POST['image'];
            $array = array(
                ":nome" => $nome,
                ":descricao" => $descricao,
                ":quantidade" => $quantidade,
                ":preco" => $preco,
                ":categoria" => $categoria,
                ":image" => $image
            );
            $product->save($array);
            break;
        case "/del-product?id={$_GET['id']}" :
            $id = $_GET['id'];
            $product->delete($id);
            break;
        case "/edit-product?sku={$_GET['sku']}":
            $id = $_GET['sku'];
            $product->edit($id);
            break;
        case "/categories":
            $category->load();
            break;
        case "/new-category":
            $category->new();
            break;
        case "/save-category":
            $nome = $_POST['nome'];
            $nome = [":nome" => $nome];
            $category->save($nome);
            break;
        case "/del-category?id={$_GET['id']}" :
            $id = $_GET['id'];
            $category->delete($id);
            break;
        case "/edit-category?id={$_GET['id']}":
            $id = $_GET['id'];
            $category->edit($id);
            break;
        case "/update-category":
            $id = $_POST['id'];
            $nome = $_POST['nome'];
            $a = array(":id" => $id, ":nome" => $nome);
            $category->update($a);
            break;
        default:
            break;
    }
} else {
    switch ($urlPath) {
        case "/install":
            require __DIR__ . '/../../views/install/database.php';
            break;
        case "/importar":
            require __DIR__ . '/../../views/install/importaProdutos.php';
        case "/padrao":
            
            $a = new Database();
            $v = $a->setProduct();
//            require __DIR__ . '/../../views/install/padrao.php';
            break;
        default:
            header("Location: /install");
            break;
    }
}