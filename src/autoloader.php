<?php

spl_autoload_register(function($class){
    $controller = __DIR__.'/controller/'.$class.'.php';
    $entity = __DIR__.'/constructor/'.$class.'.php';   


    if(!file_exists($controller)){
        return false;
    }else{
    require $controller;
    }
    if(!file_exists($entity)){
        return false;
    }else{
    require $entity;
    }
    
});