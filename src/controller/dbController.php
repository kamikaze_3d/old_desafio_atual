<?php

class dbController
{
    public $db = __DIR__.'/../../config/database/'.'gojump.sqlite';
    public $img;
    
   
    
    function verificaInstalacao() {
        if(is_file($this->db)){
            header("Location: /dashboard");
        }else{     
       header("Location: /install");
       }
    }
    
    function criarTabela() {
        if(is_file($this->db)){
            echo "o arquivo ja existe";
            header("Location: /");
        }else{     
            $this->conexao();
            $query = "CREATE TABLE IF NOT EXISTS products(nome  VARCHAR(30) NOT NULL, sku INTEGER PRIMARY KEY AUTOINCREMENT ,descricao  text,quantidade varchar  NOT NULL,preco NUMERIC(7,2) NOT NULL,categoria  text , image varchar(150) default 'tenis-basket-light.png' )";
            $db->exec($query);
       }
       header("Location: /");
    }
    
    function conexao(){
        $banco = new PDO('sqlite:'.$this->db);
        return $banco;
    }
    
    function imgRand(){
        $input = array('01.png', '02.png', '03.png', '04.png');
        $rand_keys = array_rand($input,1);
        var_dump($input); 
        $this->img = $input[$rand_keys];
    }
    
    function importaProduto() {
        
        $db = $this->conexao();
        
        
        $query1 = "CREATE TABLE IF NOT EXISTS produtos( nome VARCHAR(59) NOT NULL,img VARCHAR(255),sku  VARCHAR(9) NOT NULL PRIMARY KEY,descricao  VARCHAR(95) NOT NULL,quantidade INTEGER  NOT NULL,preco NUMERIC(7,2) NOT NULL,categorias  VARCHAR(26) NOT NULL);";
        
        
        $query2 ="INSERT INTO produtos(nome,img,sku,descricao,quantidade,preco,categorias) VALUES ('Eplerenone','01.png','42254-011','Bypass Innominate Artery to Bilateral Upper Leg Artery with Synthetic Substitute, Open Approach',59,2300.39,'Comedy|Horror|Thriller');";
        $query3 ="INSERT INTO produtos(nome,img,sku,descricao,quantidade,preco,categorias) VALUES ('Covergirl ','02.png','22700-149','Resection of Left Lower Arm and Wrist Tendon, Open Approach',38,3657.53,'Documentary');";
        $query4 ="INSERT INTO produtos(nome,img,sku,descricao,quantidade,preco,categorias) VALUES ('Clonidine Hydrochloride','03.png','0517-0730','Restriction of Splenic Vein with Extraluminal Device, Open Approach',50,4866.35,'Documentary');";
        $query5 ="INSERT INTO produtos(nome,img,sku,descricao,quantidade,preco,categorias) VALUES ('Intelence','02.png','59676-572','Drainage of Right Foot Muscle, Percutaneous Approach, Diagnostic',38,3172.88,'Comedy');";
        $query6 ="INSERT INTO produtos(nome,img,sku,descricao,quantidade,preco,categorias) VALUES ('Chorionic Gonadotropin','04.png','63323-025','Division of Left Lower Leg Muscle, Percutaneous Endoscopic Approach',97,4042.02,'Horror');";
        
        $db->query($query1);        
        
        $db->query($query2);
        $db->query($query3);
        $db->query($query4);
        $db->query($query5);
        $db->query($query6);
       
        header("Location: /");
    }
}