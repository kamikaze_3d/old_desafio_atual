<?php
require __DIR__."/../constructor/Database.php";

class CategoryController {
    function load(){
        $con = new Database();
        $allCat = $con->allCategories();
        require __DIR__."/../../views/category/category.php";
        return;
    }
    function edit($id){
        $con = new Database();
        $query = $con->editCategory($id);
        require __DIR__."/../../views/category/categoryForm.php";
    }
    function new(){
        require __DIR__."/../../views/category/categoryForm.php";
    }
    function delete($id){
        $con = new Database();
        $query = $con->delCategory($id);
        var_dump($query);
        return $query;
    }
    function save($array) {
        $con = new Database();
        $con->createCategory($array);
        header("Location: /categories");
    }
    function update($a) {
         try {
        $con = new Database();
        $con->upCategory($a);
        var_dump($con);
        header("Location: /categories");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }
}
