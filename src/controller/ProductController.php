<?php

class ProductController {
    function home(){
         $con = new Database();
        $allProd = $con->allProducts();
        require __DIR__."/../../views/product/dashboard.php";
        return;
    }
    function load(){
        $con = new Database();
        $allProd = $con->allProducts();
        require __DIR__."/../../views/product/products.php";
    }
    function edit($id){
        $con = new Database();
        $query = $con->editProduct($id);
        echo '<pre>';
        print_r($query);
        echo '</pre>';
        require __DIR__."/../../views/product/productForm.php";
    }
    function new(){
        require __DIR__."/../../views/product/productForm.php";
    }
    function delete($id){
        $con = new Database();
        $query = $con->delProduct($id);
        var_dump($query);
        return $query;
    }
    function save($array) {
        $con = new Database();
        $con->createProduct($array);
        header("Location: /products");
    }
    function update($a) {
         try {
        $con = new Database();
        $con->upProduct($a);
        var_dump($con);
        header("Location: /products");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }
}
