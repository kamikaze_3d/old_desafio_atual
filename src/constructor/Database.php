<?php

class Database {

    private $db = __DIR__ . "/../../config/database/gojump.sqlite";

    function con() {
        try {
                $connect = new PDO('sqlite:' . $this->db);
                $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $connect;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    function setCategory() {
        try {
            $db = $this->con();
            $query = $db->prepare("﻿CREATE TABLE IF NOT EXISTS categories(id INTEGER increment PRIMARY KEY , nome  VARCHAR(30) NOT NULL  )");
            $query->execute();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function createCategory($a) {
        try {
            $db = $this->con();
            $query = $db->prepare("INSERT INTO categories(nome) values(:nome)");
            $query->execute($a);
            
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function allCategories() {
         try {
        $db = $this->con();
        $query = $db->query("select * from categories");
        $query->execute();
        $allCat = $query->fetchAll();
        return $allCat;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function editCategory($id){
        try {
        $db = $this->con();
        $consulta = $db->query("SELECT nome, id FROM categories where id = {$id};");
        $linha = $consulta->fetch(PDO::FETCH_ASSOC);  
        return $linha;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
 
    }
    
    function upCategory($a) {
        try {
            $db = $this->con();
            $query = $db->query("update categories set nome = :nome where id = :id");
            $query->execute($a);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function delCategory($id){
        try {
        $db = $this->con();
        $query = $db->prepare('DELETE FROM categories WHERE id = :id');
        $query->bindParam(':id', $id); 
        $query->execute();
        header("Location: /categories");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
//products 
    
    function setProduct() {
        try {
            $db = $this->con();
            $query = $db->prepare("CREATE TABLE IF NOT EXISTS products(nome  VARCHAR(30) NOT NULL, sku INTEGER PRIMARY KEY AUTOINCREMENT ,descricao  text,quantidade varchar  NOT NULL,preco NUMERIC(7,2) NOT NULL,categoria  text , image varchar(150) default 'tenis-basket-light.png' )");
            $query->execute();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
function createProduct($a) {
        try {
            $db = $this->con();
            $query = $db->prepare("INSERT INTO products(nome,preco,descricao,quantidade,categoria, image ) "
                    . "values(:nome,:preco,:descricao,:quantidade,:categoria,:image)");
            $query->execute($a);
            echo '<pre>';
            print_r($query);            
            print_r($a);
            echo '</pre>';
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function allProducts() {
         try {
        $db = $this->con();
        $query = $db->query("select * from products");
        $query->execute();
        $allCat = $query->fetchAll();
        return $allCat;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function editProduct($id){
        try {
        $db = $this->con();
        $consulta = $db->query("SELECT nome,preco,descricao,quantidade,categoria, image FROM categories where sku = {$id};");
        $linha = $consulta->fetch(PDO::FETCH_ASSOC);  
        return $linha;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
 
    }
    
    function upProduct($a) {
        try {
            $db = $this->con();
            $query = $db->query("update products set nome = :nome, preco = :preco, descricao = :descricao, quantidade = :quantidade, categoria = :categoria, image = :image where sku = :sku");
            $query->execute($a);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function delProduct($id){
        try {
        $db = $this->con();
        $query = $db->prepare('DELETE FROM products WHERE sku = :sku');
        $query->bindParam(':sku', $id); 
        $query->execute();
        header("Location: /categories");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    function importaProduto() {
        
        
        try {
            $this->setProduct();  
            
        $query1 = "﻿CREATE TABLE IF NOT EXISTS products(nome  VARCHAR(30) NOT NULL, sku VARCHAR(9) NOT NULL PRIMARY KEY , descricao  text,quantidade INTEGER  NOT NULL,preco NUMERIC(72) NOT NULL,categoria  text references categories(nome)  on update cascade on delete cascade, image varchar(150) default 'tenis-basket-light.png' )";
        
        
        $db = $this->con();
        $query = $db->prepare($query1);
        
        
        $query2 ="INSERT INTO produtos(nome, sku, descricao, quantidade, preco, categoria references categories(nome), image) "
                . "VALUES ('Eplerenone','42254-011','Bypass Innominate Artery to Bilateral Upper Leg Artery with Synthetic Substitute, Open Approach',59,2300.39,'Comedy|Horror|Thriller','tenis-2d-shoes.png');";
        $query3 ="INSERT INTO produtos(nome, sku, descricao, quantidade, preco, categoria references categories(nome), image) "
                . "VALUES ('Covergirl ','22700-149','Resection of Left Lower Arm and Wrist Tendon, Open Approach',38,3657.53,'Documentary','tenis-basket-light.png');";
        $query4 ="INSERT INTO produtos(nome, sku, descricao, quantidade, preco, categoria references categories(nome), image) "
                . "VALUES ('Clonidine Hydrochloride','0517-0730','Restriction of Splenic Vein with Extraluminal Device, Open Approach',50,4866.35,'Documentary','tenis-basket-light.png');";
        $query5 ="INSERT INTO produtos(nome, sku, descricao, quantidade, preco, categoria references categories(nome), image) "
                . "VALUES ('Intelence','59676-572','Drainage of Right Foot Muscle, Percutaneous Approach, Diagnostic',38,3172.88,'Comedy','tenis-basket-light.png');";
        $query6 ="INSERT INTO produtos(nome, sku, descricao, quantidade, preco, categoria references categories(nome), image) "
                . "VALUES ('Chorionic Gonadotropin','63323-025','Division of Left Lower Leg Muscle, Percutaneous Endoscopic Approach',97,4042.02,'Horror','tenis-basket-light.png');";
        
        $db->query($query1);        
        
        $db->query($query2);
        $db->query($query3);
        $db->query($query4);
        $db->query($query5);
        $db->query($query6);
        
         $query = $db->query($query1);
        $query->execute();
        header("Location: /categories");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
       
        header("Location: /");
    }

} 